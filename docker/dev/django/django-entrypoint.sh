#!/usr/bin/env bash

until cd src
do
    echo "Waiting for django volume..."
done

until python manage.py migrate --settings=newsbot.settings
do
    echo "Waiting for postgres ready..."
    sleep 2
done

python manage.py collectstatic --settings=newsbot.settings
python manage.py runserver 0.0.0.0:8000 --settings=newsbot.settings
