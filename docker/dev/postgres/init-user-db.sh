#!/bin/bash

set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER bot WITH PASSWORD 'password' CREATEDB;
    CREATE DATABASE bot_db;
    GRANT ALL PRIVILEGES ON DATABASE bot_db TO bot;
EOSQL
